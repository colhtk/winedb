Data copied from https://en.wikipedia.org/wiki/List_of_wine-producing_regions on May 8th, 2017

L1: Africa
L2: Algeria
L3: Algiers Province
L3: Béjaïa Province
L3: Chlef Province
L4: Dahra, Algeria
L3: Mascara Province
L3: Médéa Province
L3: Tlemcen Province
L3: Zaccar
L2: Cape Verde
L3: Chã das Caldeiras
L2: Morocco
L3: Atlas Mountains
L2: South Africa
L3: Breede River Valley
L3: Constantia
L3: Durbanville
L3: Elgin
L3: Elim
L3: Franschhoek
L3: Little Karoo
L3: Orange River Valley
L3: Paarl
L3: Robertson
L3: Stellenbosch
L3: Swartland
L3: Tulbagh
L2: Tunisia
L3: Arianah
L3: Nabul
L3: Sousse
L1: Americas
L2: Argentina
L3: Buenos Aires Province
L4: Médanos
L3: Catamarca Province
L3: La Rioja Province
L3: Mendoza Province
L3: Neuquén Province
L3: Río Negro Province
L3: Salta Province
L3: San Juan Province
L2: Bolivia
L3: Tarija Department
L2: Brazil
L3: Bahia
L4: Curaçá
L4: Irecê
L4: Juazeiro
L3: Mato Grosso
L4: Nova Mutum
L3: Minas Gerais
L4: Andradas
L4: Caldas
L4: Pirapora
L4: Santa Rita de Caldas
L3: Paraná
L4: Bandeirantes
L4: Marialva
L4: Maringá
L4: Rosário do Avaí
L3: Pernambuco
L4: Casa Nova
L4: Petrolina
L4: Santa Maria da Boa Vista
L3: Rio Grande do Sul
L4: Bento Gonçalves
L4: Caxias do Sul
L4: Cotiporã
L4: Garibaldi
L3: Santa Catarina
L4: Pinheiro Preto
L4: São Joaquim
L4: Tangará
L3: São Paulo
L4: Jundiaí
L4: São Roque
L2: Canada
L3: British Columbia
L4: Fraser Valley
L4: Gulf Islands
L4: Okanagan Valley
L4: Similkameen Valley
L4: Vancouver Island
L3: Nova Scotia
L4: Annapolis Valley
L3: Ontario
L4: Lake Erie North Shore and Pelee Island
L4: Niagara Peninsula
L4: Prince Edward County
L4: Toronto
L3: Quebec
L4: Eastern Townships
L2: Chile
L3: Aconcagua (wine region)
L4: Aconcagua Valley
L4: Casablanca Valley
L3: Atacama
L4: Copiapó Valley
L4: Huasco Valley
L3: Central Valley
L4: Cachapoal Valley
L4: Maipo Valley
L4: Mataquito Valley
L4: Maule Valley
L3: Coquimbo
L4: Choapa Valley
L4: Elqui Valley
L4: Limarí
L3: Pica
L3: Southern Chile
L4: Bío-Bío Valley
L4: Itata Valley
L4: Malleco Valley
L2: Mexico
L3: Aguascalientes (state)
L4: Aguascalientes Valley
L3: Baja California
L4: Valle de Guadalupe
L5: Valle de Calafia
L5: Valle de Mexicali
L5: Valle de San Vicente
L5: Valle de Santo Tomás
L5: Zona Tecate
L3: La Laguna wine region
L4: Valle de Parras
L3: Guanajuato
L4: San Miguel de Allende, Dolores Hidalgo
L3: Hidalgo (state)
L3: Nuevo León
L4: Valle de Las Maravillas
L3: Querétaro
L4: Valle de Tequisquiapan
L3: Sonora
L4: Caborca
L4: Hermosillo
L3: Zacatecas
L4: Valle de las Arcinas
L2: Peru
L3: Arequipa region
L3: Lima
L4: Huaral District
L4: Cañete Province
L3: Ica Region
L4: Chincha Valley
L4: Pisco  Valley
L4: Ica Valley
L3: Pica
L2: United States
L3: Arizona
L3: Arkansas
L4: Altus
L3: California
L4: Amador County
L4: Monterey
L4: El Dorado County, California
L4: Lake County, California
L4: Livermore Valley
L4: Anderson Valley
L4: Napa Valley
L4: Ramona Valley
L4: San Joaquin County
L4: San Luis Obispo County
L4: Santa Clara Valley
L4: Santa Cruz Mountains
L4: Santa Ynez Valley
L4: Sonoma Valley
L4: Temecula Valley
L3: Colorado
L4: Palisade/ Grand Valley
L3: Florida
L3: Georgia
L3: Illinois
L3: Maine
L3: Michigan
L4: Lake Michigan Shore
L4: Leelanau Peninsula
L4: Old Mission Peninsula
L3: Minnesota
L4: Alexandria Lakes
L4: Upper Mississippi Valley
L3: Missouri
L4: Augusta
L4: Hermann
L4: Ozark Highlands
L4: Ozark Mountains (also in Oklahoma and Arkansas)
L4: Ste. Genevieve
L3: New Jersey
L3: New Mexico
L4: Albuquerque
L4: Deming
L4: La Union
L4: Las Cruces
L4: Santa Fe
L4: Taos
L3: New York
L4: Long Island
L4: Finger Lakes
L4: Hudson River Valley
L4: Lake Erie Region
L4: Niagara Escarpment
L3: North Carolina
L4: Yadkin Valley
L3: Ohio
L4: Grand River Valley
L4: Isle St. George
L4: Kanawha River Valley
L4: Loramie Creek
L4: Ohio River Valley
L3: Oklahoma
L3: Oregon
L4: Applegate Valley
L4: Columbia Valley
L4: Hood River County
L4: Rogue Valley
L4: Umpqua Valley
L4: Willamette Valley
L4: Walla Walla Valley
L3: Pennsylvania
L4: Central Delaware Valley
L4: Cumberland Valley
L4: Erie County
L4: Lancaster County
L4: Lehigh County
L3: Texas
L4: Bell Mountain
L4: Davis Mountains
L4: Escondido Valley
L4: Fredericksburg
L4: Llano Estacado
L4: Texas Hill Country
L3: Virginia
L4: Monticello
L4: North Fork of Roanoke
L4: Northern Neck George Washington Birthplace
L4: Rocky Knob
L4: Shenandoah Valley
L4: Virginia's Eastern Shore
L3: Washington
L4: Columbia Gorge
L4: Columbia Valley
L5: Horse Heaven Hills
L5: Naches Heights
L5: Rattlesnake Hills
L5: Red Mountain
L5: Wahluke Slope
L5: Walla Walla Valley
L5: Yakima Valley
L4: Puget Sound
L3: Wisconsin
L4: Niagara Escarpment
L2: Uruguay
L2: Venezuela
L3: Carora, Lara State
L1: Europe
L2: Albania
L3: Berat
L3: Korça
L3: Leskovik
L3: Lezhë
L3: Përmet
L3: Shkoder
L3: Tirana County
L2: Armenia
L3: Ararat Valley
L3: Vayots Dzor Province
L4: Areni
L3: Tavush Province
L4: Ijevan
L2: Austria
L3: Burgenland
L3: Northeastern and eastern Lower Austria
L4: Kamptal
L4: Kremstal
L4: Wachau
L4: Wagram
L4: Weinviertel
L3: Southern Styria
L3: Vienna
L2: Azerbaijan
L3: Baku, capital
L3: Ganja
L3: Madrasa
L3: Tovuz
L3: Shamkir
L2: Belgium
L3: Côtes de Sambre et Meuse
L3: Hagelandse wijn
L3: Haspengouw
L3: Heuvelland
L2: Bosnia and Herzegovina
L3: Čapljina
L3: Čitluk
L3: Ljubuški
L3: Međugorje
L3: Mostar
L3: Stolac
L3: Trebinje
L2: Bulgaria
L3: Black Sea region
L3: Danubian Plain
L3: Rose Valley
L3: Thrace
L3: Valley of the Struma River
L2: Croatia
L3: Continental Croatia: Central Croatia and Slavonia
L4: Moslavina
L4: Plešivica
L4: Podunavlje
L4: Pokuplje
L4: Prigorje - Bilogora
L4: Slavonia
L4: Zagorje - Međimurje
L3: Littoral Croatia: Northern Croatian Littoral and Dalmatia
L4: Croatian Coast (Hrvatsko primorje)
L4: Dalmatian Interior (Dalmatinska zagora)
L4: Central and South Dalmatia (Srednja i Južna Dalmacija)
L4: Northern Dalmatia (Sjeverna Dalmacija)
L4: Istria (Istra)
L2: Cyprus
L3: Commandaria
L3: Laona - Akamas
L3: Vouni Panagias – Ambelitis
L3: Krasochoria Lemesou
L3: Pitsilia
L3: Diarizos Valley
L2: Czech Republic
L3: Moravia
L4: Mikulov - Mikulovska wine
L4: Slovácko
L4: Velké Pavlovice
L4: Znojmo
L3: Bohemia
L4: Litoměřice
L4: Mělník
L3: Prague
L4: Gazebo at Gröbe’s Villa
L4: St. Clare’s
L4: St. Wenceslas’ Vineyard at Prague castle
L4: Salabka, Troja
L2: Denmark
L2: France
L3: Alsace
L3: Bordeaux
L4: Barsac
L4: Entre-Deux-Mers
L4: Fronsac
L4: Graves
L4: Haut-Médoc
L4: Margaux
L4: Médoc
L4: Pauillac
L4: Pessac-Léognan
L4: Pomerol
L4: Saint-Émilion
L4: Saint-Estèphe
L4: Saint-Julien
L4: Sauternes
L3: Burgundy
L4: Beaujolais
L4: Bugey
L4: Chablis
L4: Côte Chalonnaise
L4: Côte d'Or
L5: Côte de Beaune
L6: Aloxe-Corton
L6: Auxey-Duresses
L6: Beaune
L6: Chassagne-Montrachet
L6: Meursault
L6: Santenay
L5: Côte de Nuits
L6: Chambolle-Musigny
L6: Gevrey-Chambertin
L6: Nuits-Saint-Georges
L6: Vosne-Romanée
L4: Mâconnais
L5: Pouilly-Fuissé
L3: Champagne
L3: Jura
L3: Languedoc-Roussillon
L4: Banyuls
L4: Blanquette de Limoux
L4: Cabardes
L4: Collioure
L4: Corbières
L4: Côtes du Roussillon
L4: Fitou
L4: Maury
L4: Minervois
L4: Rivesaltes
L3: Loire Valley
L4: Saumur
L4: Cognac
L4: Muscadet
L4: Pouilly Fumé
L4: Sancerre
L4: Touraine
L3: Lorraine
L3: Madiran
L3: Provence
L3: Rhône
L4: Beaumes-de-Venise
L4: Château-Grillet
L4: Châteauneuf-du-Pape
L4: Condrieu
L4: Cornas
L4: Côte du Rhône-Villages, Rhône wine
L4: Côte-Rôtie
L4: Côtes du Rhône
L4: Crozes-Hermitage
L4: Gigondas
L4: Hermitage
L4: St. Joseph
L4: Saint-Péray
L4: Vacqueyras
L3: Savoie
L2: Georgia
L3: Abkhazia
L3: Kakheti
L4: Telavi
L4: Kvareli
L3: Kartli
L3: Imereti
L3: Racha-Lechkhumi
L3: Kvemo Svaneti
L2: Germany
L3: Ahr
L3: Baden
L3: Franconia
L3: Hessische Bergstraße
L3: Mittelrhein
L3: Mosel
L3: Nahe
L3: Palatinate
L3: Rheingau
L3: Rheinhessen
L3: Saale-Unstrut
L3: Saxony
L3: Württemberg
L2: Greece
L3: Aegean islands
L4: Crete
L4: Limnos
L4: Paros
L4: Rhodes
L4: Samos
L4: Santorini
L3: Central Greece
L4: Attica
L4: Epirus
L5: Zitsa
L4: Thessaly
L5: Nea Anchialos
L5: Rapsani
L3: Ionian Islands
L4: Kefalonia
L3: Macedonia
L4: Amyntaion
L4: Goumenissa
L4: Naousa, Imathia
L3: Peloponnesus
L4: Mantineia
L4: Nemea
L4: Patras
L2: Hungary
L3: Balaton
L4: Badacsony
L4: Balaton-felvidék
L4: Balatonboglár
L4: Balatonfüred-Csopak
L4: Nagy-Somló
L4: Zala
L3: Duna
L4: Csongrád
L4: Hajós-Baja
L4: Kunság
L3: Eger
L4: Bükk
L4: Eger
L4: Észak-Dunántúl
L4: Etyek-Buda
L4: Mátra
L4: Mór
L4: Neszmély
L4: Pannonhalma
L3: Pannon
L4: Pécs
L4: Szekszárd
L4: Tolna
L4: Villány
L3: Sopron
L3: Tokaj
L2: Ireland
L3: Cork
L2: Italy
L3: Apulia
L4: Bianco di Locorotondo e Martina Franca
L4: Primitivo di Manduria
L3: Calabria
L4: Bivongi
L4: Cirò
L4: Gaglioppo
L4: Greco di Bianco
L4: Lamezia
L4: Melissa
L4: Sant'Anna di Isola Capo Rizzuto
L4: Savuto
L4: Scavigna
L4: Terre di Cosenza
L3: Emilia-Romagna
L4: Colli Cesenate
L4: Sangiovese Superiore di Romagna
L4: Trebbiano di Romagna
L3: Liguria
L4: Cinque Terre
L3: Lombardy
L4: Franciacorta
L4: Oltrepo Pavese
L3: Marche
L4: Castelli di Jesi
L4: Conero
L4: Piceno
L3: Piedmont
L4: Acqui
L4: Alba
L4: Asti
L4: Barolo
L4: Colli Tortonesi
L4: Gattinara
L4: Gavi
L4: Ghemme
L4: Langhe
L4: Monferrato
L4: Nizza
L4: Ovada
L3: Sardinia
L4: Cagliari
L4: Cannonau
L4: Monti
L4: Nuragus
L4: Ogliastra
L4: Vermentino di Gallura
L3: Sicily
L4: Etna
L4: Noto
L4: Pantelleria
L3: Trentino-Alto Adige
L4: South Tyrol
L3: Tuscany
L4: Bolgheri
L4: Chianti
L4: Chianti Classico
L4: Colli Apuani
L4: Colli Etruria Centrale
L4: Colline Lucchesi
L4: Elba
L4: Montalcino
L4: Montescudaio
L4: Parrina
L4: Pitigliano
L4: San Gimignano
L4: Scansano
L4: Val di Chiana
L4: Val di Cornia
L4: Valdinievole
L4: Valle di Arbia
L3: Umbria
L4: Montefalco
L4: Orvieto
L4: Torgiano
L3: Veneto
L4: Arcole
L4: Bagnoli
L4: Bardolino
L4: Bianco di Custoza
L4: Breganze
L4: Colli Berici
L4: Colli di Conegliano
L4: Colli Euganei
L4: Gambellara
L4: Garda
L4: Lessini Durello
L4: Lison Pramaggiore
L4: Lugana
L4: Montgello e Colli Asolani
L4: Piave
L4: Prosecco di Conegliano - Valdobbiadene
L4: Soave
L4: Valdadige
L4: Valpolicella
L2: Latvia
L3: Sabile
L2: Luxembourg
L3: Moselle Valley
L2: Macedonia
L3: Povardarie
L3: Skopsko vinogorje
L3: Tikveš
L2: Moldova
L3: Bardar
L3: Codri
L3: Cricova
L3: Hînceşti
L3: Purcari
L2: Montenegro
L3: Crmnica
L3: Plantaže
L2: Netherlands
L3: Groesbeek
L2: Poland
L3: Dolny Śląsk
L3: Kazimierz Dolny
L3: Małopolska
L3: Podkarpackie Voivodeship
L3: Warka, near Warsaw
L3: Zielona Góra
L2: Portugal
L3: Alentejo
L3: Bairrada
L3: Bucelas
L3: Carcavelos
L3: Colares
L3: Dão
L3: Lagoa
L3: Lagos
L3: Madeira
L3: Portimão
L3: Porto e Douro
L3: Setúbal
L3: Tavira
L3: Vinhos Verdes
L2: Romania
L3: Banat wine regions:
L4: Arad
L4: Jamu Mare
L4: Măderat
L4: Miniş
L4: Moldova Nouă
L4: Recaş
L4: Silagiu
L4: Teremia
L4: Tirol
L3: Crişana wine regions:
L4: Diosig
L4: Săcuieni
L4: Sâniob
L4: Sanislău
L4: Valea lui Mihai
L3: Dobrogea wine regions:
L4: Adamclisi
L4: Babadag
L4: Aliman
L4: Băneasa
L4: Cernavodă
L4: Chirnogeni
L4: Dăeni
L4: Hârşova
L4: Istria
L4: Măcin
L4: Medgidia
L4: Murfatlar
L4: Oltina
L4: Ostrov, Constanţa
L4: Ostrov, Tulcea
L4: Poarta Albă
L4: Sarica-Niculiţel
L4: Simioc
L4: Tulcea
L4: Valea Dacilor
L4: Valea Nucarilor
L4: Valu lui Traian
L3: Moldavia wine regions:
L4: Bereşti
L4: Bohotin
L4: Coteşti
L4: Colinele Tutovei
L4: Comarna
L4: Copou
L4: Corod
L4: Cotnari
L4: Covurlui
L4: Cucuteni
L4: Dealu Morii
L4: Dealul Bujorului
L4: Hârlău
L4: Hlipicani
L4: Huşi
L4: Iveşti
L4: Iaşi
L4: Jariştea
L4: Nămoloasa
L4: Nicoreşti
L4: Odobeşti
L4: Panciu
L4: Păuneşti
L4: Probota
L4: Tănăsoaia
L4: Târgu Frumos
L4: Tecuci
L4: Ţifeşti
L4: Tomeşti
L4: Vaslui
L4: Zeletin
L3: Muntenia wine regions:
L4: Seciu
L4: Breaza
L4: Cricov
L4: Dealu Mare
L4: Dealurile Buzăului
L4: Pietroasa
L4: Râmnicu Sărat
L4: Sercaia
L4: Ştefăneşti
L4: Tohani
L4: Topoloveni
L4: Urlaţi - Ceptura
L4: Valea Călugărească
L4: Valea Mare
L4: Zărneşti
L4: Zoreşti
L3: Oltenia wine regions:
L4: Banu Mărăcine
L4: Calafat
L4: Cetate
L4: Corcova
L4: Dăbuleni
L4: Dealul Viilor
L4: Dealurile Craiovei
L4: Drăgăşani
L4: Golul Drincei
L4: Greaca
L4: Iancu Jianu
L4: Izvoarele
L4: Oreviţa
L4: Plaiurile Drâncei
L4: Pleniţa
L4: Podgoria Dacilor
L4: Podgoria Severinului
L4: Poiana Mare
L4: Potelu
L4: Sadova-Corabia
L4: Sâmbureşti
L4: Segarcea
L4: Tâmbureşti
L4: Vânju Mare
L4: Zimnicea
L3: Transylvania wine regions:
L4: Aiud
L4: Alba Iulia
L4: Bistriţa
L4: Blaj
L4: Ighiu
L4: Jidvei
L4: Lechinta
L4: Mediaş
L4: Şamşud
L4: Sebeş-Apold
L4: Şimleul Silvaniei
L4: Târnave
L4: Târnăveni
L4: Teaca
L4: Triteni
L4: Valea Nirajului
L2: Russia
L3: Caucasus
L3: Krasnodar
L3: Stavropol
L2: Serbia
L3: Banat region
L3: Kosovo region
L3: Nišava - South Morava region
L3: Pocerina region
L3: Srem region
L3: Subotica - Horgoš region
L3: Šumadija - Great Morava region
L3: Timočka Krajina
L3: West Morava region
L2: Slovakia
L3: Malokarpatská (Small Carpathians)
L3: Južnoslovenská (Southern Slovakia)
L3: Nitrianska (region of Nitra)
L3: Stredoslovenská (Central Slovakia)
L3: Tokaj (Tokaj region of Slovakia)
L3: Východoslovenská (Eastern Slovakia)
L3: The whole of southern Slovakia
L2: Slovenia
L3: Podravje
L3: Posavje
L3: Primorska
L2: Spain
L3: Andalusia
L4: Condado de Huelva
L4: Jerez-Xeres-Sherry
L4: Málaga and Sierras de Málaga
L4: Manzanilla de Sanlúcar de Barrameda
L4: Montilla-Moriles
L3: Aragon
L4: Calatayud
L4: Campo de Borja
L4: Campo de Cariñena
L4: Cava
L4: Somontano
L3: Balearic Islands
L4: Binissalem-Mallorca
L4: Plà i Llevant (DO)
L3: Basque Country
L4: Alavan Txakoli
L4: Biscayan Txakoli
L4: Cava
L4: Getaria Txakoli
L4: Rioja (Alavesa)
L3: Canary Islands
L4: Abona
L4: El Hierro (DO)
L4: Gran Canaria (DO)
L4: La Gomera (DO)
L4: La Palma (DO)
L4: Lanzarote (DO)
L4: Tacoronte-Acentejo
L4: Valle de Güímar
L4: Valle de la Orotava
L4: Ycoden-Daute-Isora
L3: Castile and León
L4: Arlanza
L4: Arribes del Duero
L4: Bierzo
L4: Cava
L4: Cigales
L4: Espumosos de Castilla y León
L4: Ribera del Duero
L4: Rueda
L4: Tierra del Vino de Zamora
L4: Toro
L4: Valles de Benavente
L4: Tierra de León
L4: Valtiendas
L4: Vino de la Tierra Castilla y León
L3: Castile–La Mancha
L4: Almansa
L4: Dominio de Valdepusa
L4: Guijoso
L4: Jumilla
L4: La Mancha
L4: Manchuela
L4: Méntrida
L4: Mondéjar
L4: Ribera del Júcar
L4: Valdepeñas
L3: Catalonia
L4: Alella
L4: Catalunya
L4: Cava
L4: Conca de Barberà
L4: Costers del Segre
L4: Empordà
L4: Montsant
L4: Penedès
L4: Pla de Bages
L4: Priorat
L4: Tarragona
L4: Terra Alta
L3: Extremadura
L4: Cava
L4: Ribera del Guadiana
L3: Galicia
L4: Monterrey
L4: Rías Bajas
L4: Ribeira Sacra
L4: Ribeiro
L4: Valdeorras
L3: La Rioja
L4: Cava
L4: Rioja (DOCa)
L3: Community of Madrid
L4: Vinos de Madrid
L3: Región de Murcia
L4: Bullas
L4: Jumilla
L4: Yecla
L3: Navarre
L4: Cava
L4: Navarra
L4: Rioja
L3: Valencian Community
L4: Alicante
L4: Cava
L4: Utiel-Requena
L4: Valencia
L2: Sweden
L3: Gutevin - Gotland
L2: Switzerland
L3: Aargau
L3: Bern
L4: Shores of Lake Biel
L4: Shores of Lake Thun (Spiez / Oberhofen)
L3: Freiburg
L3: Geneva
L3: Grisons
L3: Neuchâtel
L3: St. Gallen
L3: Schaffhausen
L3: Thurgau
L3: Ticino
L3: Valais
L3: Vaud
L4: La Côte
L4: Lavaux
L3: Zürich
L2: Turkey
L3: Marmara wine region
L4: Kırklareli Province
L4: Tekirdağ Province
L4: Balıkesir Province
L4: Istanbul Province
L3: Aegean wine region
L4: Çanakkale Province
L4: Manisa Province
L4: Denizli Province
L4: İzmir Province
L4: Isparta Province
L4: Muğla Province
L4: Aydın Province
L4: Burdur Province
L3: Mediterranean wine region
L4: Antalya Province
L4: Mersin Province
L3: Mid-southern Anatolia wine region
L4: Nevşehir Province
L4: Konya Province
L3: Mid-northern Anatolia wine region
L4: Ankara Province
L4: Eskişehir Province
L4: Yozgat Province
L4: Sivas Province
L4: Çankarı Province
L3: Mid-eastern Anatolia wine region
L4: Tokat Province
L4: Elazığ Province
L3: Southeast Anatolia wine region
L4: Şanlıurfa Province
L4: Diyarbakır Province
L2: Ukraine
L3: Autonomous Republic of Crimea and Sevastopol - 6 macrozones with 12 microzones (69 wine grapes)
L3: Kherson Oblast
L3: Mykolaiv Oblast
L3: Odessa Oblast
L3: Zakarpattia Oblast
L3: Zaporizhia Oblast
L2: United Kingdom
L1: Asia
L2: Burma
L3: Shan State
L2: China
L3: Chang'an
L3: Gaochang
L3: Luoyang
L3: Qiuci
L3: Yantai-Penglai
L3: Chang'an
L3: Dalian, Liaoning
L3: Tonghua, Jilin
L3: Yantai, Shandong
L3: Yibin, Sichuan
L3: Zhangjiakou, Hebei
L2: India
L3: Bangalore, Karnataka
L3: Bijapur, Karnataka
L3: Narayangaon
L3: Nashik, Maharashtra
L3: Pune, Maharashtra
L3: Sangli, Maharashtra
L2: Indonesia
L3: Bali
L2: Iran
L3: Malayer
L3: Shiraz
L3: Takestan
L3: Urmia
L3: Qazvin
L3: Quchan
L2: Israel
L3: Bet Shemesh
L3: Galilee
L3: Golan Heights
L3: Jerusalem
L3: Judean Hills
L3: Latrun
L3: Mount Carmel
L3: Rishon LeZion (wine production since 1886)
L2: Japan
L3: Nagano
L3: Yamanashi
L2: Kazakhstan
L2: Republic of Korea
L3: Gyeonggi-do
L4: Anseong
L3: Gyeongsangbuk-do
L4: Gimcheon
L4: Gyeongsan
L4: Yeongcheon
L3: Chungcheongbuk-do
L4: Yeongdong
L2: Lebanon
L3: Bekaa Valley
L4: Anjar
L4: Chtoura
L4: Rashaya
L4: Zahlé
L3: Mount Lebanon
L4: Aley
L4: Baabda
L4: Beit Mery
L4: Bhamdoun
L4: Brummana
L4: Byblos
L4: Chouf
L4: Keserwan District
L3: North Governorate
L4: Chekka
L4: Ehden
L4: Koura
L4: Qadisha Valley
L4: Tripoli
L4: Zgharta
L3: South Governorate
L4: Jezzine
L4: Marjayoun
L4: Rmaich
L2: Palestinian territories
L3: Beit Jala
L3: Hebron
L2: Syria
L3: Homs District
L3: Jabal el Druze
L2: Vietnam
L3: Da Lat
L1: Oceania
L2: Australia
L2: =New South Wales=
L3: Big Rivers
L4: Murray Darling
L4: Perricoota
L4: Riverina
L4: Swan Hill
L3: Central Ranges
L4: Cowra
L4: Mudgee
L4: Orange
L3: Hunter Region
L4: Hunter Valley
L5: Broke Fordwich
L5: Pokolbin
L5: Upper Hunter Valley
L3: Northern Rivers
L4: Hastings River
L3: Northern Slopes
L4: New England Australia
L3: South Coast
L4: Shoalhaven Coast
L4: Southern Highlands
L3: Southern New South Wales
L4: Canberra District
L4: Gundagai
L4: Hilltops
L4: Tumbarumba
L2: =Queensland=
L3: Granite Belt
L3: South Burnett
L2: =South Australia=
L3: Barossa Zone
L4: Barossa Valley
L4: Eden Valley
L5: High Eden
L3: Far North
L4: Southern Flinders Ranges
L3: Fleurieu
L4: Currency Creek
L4: Kangaroo Island
L4: Langhorne Creek
L4: McLaren Vale
L4: Southern Fleurieu
L3: Limestone Coast
L4: Coonawarra
L4: Mount Benson
L4: Mount Gambier
L4: Padthaway
L4: Robe
L4: Wrattonbully
L3: Lower Murray
L4: Riverland
L3: Mount Lofty Ranges
L4: Adelaide Hills
L5: Lenswood
L5: Piccadilly Valley
L4: Adelaide Plains
L4: Clare Valley
L3: The Peninsulas
L2: =Tasmania=
L3: Coal River
L3: Derwent Valley
L3: East Coast
L3: North West
L3: Pipers River
L3: Southern
L3: Tamar Valley
L2: =Victoria=
L3: Central Victoria
L4: Bendigo
L4: Goulburn Valley
L5: Nagambie Lakes
L4: Heathcote
L4: Strathbogie Ranges
L4: Upper Goulburn
L3: Gippsland
L3: North East Victoria
L4: Alpine Valleys
L4: Beechworth
L4: Glenrowan
L4: King Valley
L4: Rutherglen
L3: North West Victoria
L4: Murray Darling
L4: Swan Hill
L3: Port Phillip
L4: Geelong
L4: Macedon Ranges
L4: Mornington Peninsula
L4: Sunbury
L4: Yarra Valley
L3: Western Victoria
L4: Grampians
L4: Henty
L4: Pyrenees
L2: =Western Australia=
L3: Greater Perth
L4: Peel
L4: Perth Hills
L4: Swan Valley
L3: South Western Australia
L4: Blackwood Valley
L4: Geographe
L4: Great Southern
L5: Albany
L5: Denmark
L5: Frankland River
L5: Mount Barker
L5: Porongurup
L4: Manjimup
L4: Margaret River
L4: Pemberton
L2: New Zealand
L3: Auckland Region
L4: Henderson
L4: Kumeu
L4: Matakana
L4: Waiheke Island
L3: Bay of Plenty
L3: Canterbury
L4: Waipara
L3: Central Otago
L4: Bendigo
L4: Wanaka
L3: Hawke's Bay region
L4: Gimblett Gravells
L3: Gisborne
L3: Marlborough
L4: Blenheim
L4: Cloudy Bay
L4: Renwick
L4: Seddon
L3: Nelson
L3: Northland Region
L3: Waikato
L4: Te Awamutu
L4: Te Kauwhata
L3: Wairarapa
L4: Martinborough
